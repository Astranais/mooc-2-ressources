## Objectifs

- S'entraîner à l'utilisation d'instructions Python simples directement dans la console
- Écrire un script et l'exécuter
- Passer progressivement de l'écriture répétitive à l'écriture sous forme de boucles
- Utiliser l'instruction conditionnelle
- Écrire des fonctions qui sont réutilisées dans différents exercices avec différents paramètres

## Pré-requis à cette activité

Connaître les différentes parties composant "l'environnement de développement" (IDE) utilisé :
- La console / l'interpréteur Python pour exécuter en direct commande par commande 
- La zone d'écriture de scripts (nécessaire à partir de la question 3)
- L'exécution d'un script

Être déjà familiarisé avec l'utilisation de Python :
- boucle for ou while (à partir de la question 8)
- instructions conditionnelles (pour les graphes endeux couleurs ?)
- utilisation des fonctions (distinguer paramètres obligatoires ou optionnels)
- écriture/définition de fonctions
- Ex 5, utiliser soit sqrt du module math ou **(1/2) 

Avoir de bonnes bases en mathématique :
- coordonnées dans un repère $\rightarrow$ préciser ou rappeler la position et l'orientation initiale de la tortue
- angles des principales figures géométriques
- théorème de Pythagore

Avoir une liste des fonction turtle nécessaire à disposition et des paramètres principaux :
- remplissage (pas forcément utile selon ce qui est voulu en Ex17)
- lever / descendre le crayon (à partir ex 12)
- backward (bien utile pour éviter des demi-tour Ex 15 et 16)

## Durée de l'activité

Entre 2 et 3 fois 2h10 (temps que j'ai pris pour le faire, ne compter que 1h10 si on enlève la partie 4*)


## Exercices cibles

| compétence | exercices cibles |
|--------|:--------:|
| utilisation de Python dans la console | 1,2,11 |
| écrire un script | 3-5,17 |
| utiliser une boucle `for` | 6-10 + |
| définir une fonction et l'appeler avec différents paramètres | 12-15,18-20 |
| utiliser une structure conditionnelle `if` | 16 |

## Description du déroulement de l'activité

### Avant le début de l'activité
Cours/explication sur les prérequis  
Précisions que les élèves n'auront besoin de rien d'autre pour réussir l'activité

### Organisation matérielle
Si l'activité est faite dans un IDE classique, alors préciser auparavant aux élèves quelques exercices ils peuvent regrouper dans le même script en commentant les exercices déjà fait (montrer un exemple):
- 2-3
- 4-5
- 6-7
- 8-11
- 12-17
- 18-20

Sinon, faire l'activité dans Jupyter en précisant à l'avance :
- qu'il faut compiler deux fois la case (raccourci clavier shift+enter)
- qu'il faut ajouter la commande `done()` à la fin de chaque case

### Organisation temporelle
Exercices 1 à 9 obligatoires à faire pendant la première heure et demi  
Exercices 12-13 puis 15-16 à faire si possible ensuite  
Partie 4* pour ceux qui se débrouillent bien et vont vite


## Anticipation des difficultés des élèves

- Ex 10+ : diminuer la taille des côtés pour continuer à voir la figure en entier
- Ex 13+ : quels paramètres mettre dans la fonction carré
- Encourager les élèves à écrire une nouvelle fonction carré pour le tracer dans l'autre sens
- Ex 15: Double boucle imbriquée
- Ex 16 : utilisation de `if`
- Ex 19 : encourager les élèves à tatonner pour trouver l'angle pour tracer l'étoile à 5 branches
- Ex 20 :
  - guider les élèves pour réfléchir à la meilleure solution pour tracer l'étoile à 6 branches selon la position finale du crayon
  - faire réaliser aux élèves que l'angle du dessin est naturel suite au tracé de l'étoile à 6 branches


## Gestion de l'hétérogénéïté

Pour les élèves ayant fini très rapidement, leur demander ensuite de reprendre l'activité depuis le début en rendant leur code le plus propre et épuré possible (créer des fonctions supplémentaires, fusionner les deux fonctions carré pour choisir le sens de tracé...)  
Pour les élèves ayant des difficultés, les débloquer régulièrement et faire en sorte qu'ils fasse au moins les exercices minimaux 
